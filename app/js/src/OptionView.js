var App = App || {};
App.OptionView = (function (options)
{
    "use strict";

    var that = {},
        modal,
        saveButton,
        notificationCheckbox,
        soundCheckbox,
        saveable;

    function init()
    {
        getElements();
        initStorage();
        saveButton.addEventListener("click", onSaveButtonClick);
        return that;
    }

    function getElements()
    {
        modal = options.modal;
        saveButton = options.closeBigButton;
        notificationCheckbox = options.notificationCheckbox;
        soundCheckbox = options.soundCheckbox;
    }

    function initStorage()
    {
        if(typeof(Storage) !== undefined)
        {
            saveable = true;
            notificationCheckbox.checked = (localStorage.getItem("notificationsAllowed") === "true");
            soundCheckbox.checked = (localStorage.getItem("soundAllowed") === "true");
        }
        else
        {
            saveable = false;
        }
    }

    function onSaveButtonClick() {
        var notificationsAllowed,
            soundAllowed;

        notificationsAllowed = notificationCheckbox.checked;
        soundAllowed = soundCheckbox.checked;

        if(saveable) {
            localStorage.setItem("notificationsAllowed", notificationsAllowed);
            localStorage.setItem("soundAllowed", soundAllowed);
        }

        modal.style.display = "none";
    }

    function getOptions() {
        var modalOptions = {};
        modalOptions.notifications = notificationCheckbox.checked;
        modalOptions.sounds = soundCheckbox.checked;
        return modalOptions;
    }

    that.init = init;
    that.getOptions = getOptions;
    return that;
});
