var App = App || {};
App.PlausibleChecker = (function(){
    "use strict";

    var that = {};

    function gameRunning(gameStatus) {
        if(gameStatus.gameStarted === null) {
            return false;
        }
        else if(gameStatus.gameStarted && !gameStatus.halftime){
            return true;
        }
        else if(gameStatus.gameStarted) {
            return true;
        }
        else {
            return false;
        }
    }

    function gameHasEnded(gameStatus) {
        if((gameStatus.gameStarted !== null && !gameStatus.halftime !== null) && (!gameStatus.gameStarted && !gameStatus.halftime)) {
            return true;
        } else {
            return false;
        }
    }

    that.gameRunning = gameRunning;
    that.gameHasEnded = gameHasEnded;
    return that;
});
