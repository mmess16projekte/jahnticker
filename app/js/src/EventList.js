var App = App || {};
App.EventList = (function (options) {
    "use strict";
    /*global EventPublisher, $, _*/

    var that = new EventPublisher(),
        eventID,
        eventList;

    function init() {
        eventList = options.eventList;
        eventID = 0;

        return that;
    }

    function clearList() {
        while (eventList.firstChild) {
            eventList.removeChild(eventList.firstChild);
        }
    }

    function onPublishButtonClick(type, tmpElement, feedData) {
        var eventInfo = {};
        eventInfo.type = type;
        eventInfo.root = options.root;
        eventInfo.id = eventID;
        eventInfo.time = tmpElement.querySelector("#eventTime").innerHTML;
        eventInfo.title = tmpElement.querySelector("#eventHeaderInput").value;
        eventInfo.text = tmpElement.querySelector("#eventTextInput").value;
        eventInfo.text = eventInfo.text.replace(/\r?\n/g, "<br />");
        eventInfo.scoreTeamOne = feedData.scoreTeamOne;
        eventInfo.scoreTeamTwo = feedData.scoreTeamTwo;
        eventInfo.playerOne = feedData.playerOne;
        eventInfo.playerTwo = feedData.playerTwo;
        eventID++;
        that.notifyAll("eventPublished", eventInfo);
    }

    function addEventListEntryByTemplate(feedData, template) {
        var bindingFunction = _.template(template),
            tmpElement,
            type,
            publishButton,
            cancelButton,
            eventIcon,
            thisListEntry;

        tmpElement = document.createElement("div");
        tmpElement.innerHTML = bindingFunction(feedData);

        publishButton = tmpElement.querySelector("#eventPublishButton");
        cancelButton = tmpElement.querySelector("#eventCancelButton");
        eventIcon = tmpElement.querySelector(".eventIcon");

        setIconForType(eventIcon, feedData.type);

        $(publishButton).click(function () {
            thisListEntry = $(this).closest(".feedEntry").get(0);
            type = $(this).closest(".feedEntry").attr("type");
            onPublishButtonClick(type, thisListEntry, feedData);
            $(this).closest(".feedEntry").fadeOut(500, function () {
                $(this).remove();
            });
        });

        $(cancelButton).click(function () {
            $(this).closest(".feedEntry").fadeOut(500, function () {
                $(this).remove();
            });
            that.notifyAll("eventCreationCanceled");
        });

        $(eventList).prepend(tmpElement.children[0]);
    }

    function setIconForType(eventIcon, type) {
        switch (type) {
        case "whistle":
            eventIcon.classList.add("whistleIcon");
            break;
        case "text":
            eventIcon.classList.add("otherIcon");
            break;
        case "goal":
            eventIcon.classList.add("goalIcon");
            break;
        case "yellow":
            eventIcon.classList.add("yellowCardIcon");
            break;
        case "red":
            eventIcon.classList.add("redCardIcon");
            break;
        case "foul":
            eventIcon.classList.add("foulIcon");
            break;
        case "chance":
            eventIcon.classList.add("chanceIcon");
            break;
        case "sub":
            eventIcon.classList.add("exchangeIcon");
            break;
        default:
            break;
        }
    }

    function setEventStart(id) {
        eventID = id;
    }

    that.init = init;
    that.addEventListEntryByTemplate = addEventListEntryByTemplate;
    that.setEventStart = setEventStart;
    that.clearList = clearList;
    return that;
});