var App = App || {};
App.JahntickerApp = (function () {
    "use strict";
    /*global $*/

    var that = {},
        mainView,
        loginView,
        loginScreen,
        scheduleScreen,
        tickerScreen,
        firebaseController,
        score,
        scoreView,
        scheduleList,
        logoutButtons,
        backButton,
        rawGamesData,
        rawTeamsData,
        computedGamesData,
        tickerView,
        timeCounter,
        teamOneList,
        teamTwoList,
        eventList,
        gameID,
        clickedIconType,
        playerOne,
        playerTwo,
        highlightPanel,
        rawDataRetrieved,
        checker,
        gameRunning = false;

    function init() {
        initViews();
        initFirebaseConnection();
        initDOMElements();
        initListeners();
        handleImpressModal();
        checker = new App.PlausibleChecker();
    }

    function initViews() {
        initMainView();
        initLoginView();
        initScheduleList();
        initTickerView();
        initPlayerLists();
        initScoreCount();
    }

    function initMainView() {
        mainView = new App.MainView();
    }

    function initLoginView() {
        loginView = (new App.LoginView({
            usernameInput: document.querySelector("input[name=userNameInput]"),
            passwordInput: document.querySelector("input[name=passwordInput]"),
            loginButton: document.querySelector("input[name=loginButton]"),
            loadingView: document.querySelector(".loadingAfterLogin"),
            inputsView: document.querySelector(".loginInputs"),
            loginErrorText: document.querySelector(".loginErrorText")
        })).init();
        loginView.initListeners();
    }

    function initScheduleList() {
        scheduleList = (new App.ScheduleList({
            entryGameTemplateContent: document.querySelector("#schedule-entry").innerHTML,
            scheduleList: document.querySelector(".scheduleList")
        })).init();
        scheduleList.setOnScheduleListGameSelectedListener(onScheduleListGameSelected);
    }

    function initTickerView() {
        tickerView = (new App.TickerView({
            teamOneNameResult: document.getElementById("teamOneName"),
            teamTwoNameResult: document.getElementById("teamTwoName"),
            teamOneLogo: document.getElementById("logoTeamOne"),
            teamTwoLogo: document.getElementById("logoTeamTwo"),
            teamOneNameLineUp: document.getElementById("teamOneNameLineUp"),
            teamOneCoachName: document.querySelector(".teamOne .trainerName"),
            teamTwoNameLineUp: document.getElementById("teamTwoNameLineUp"),
            teamTwoCoachName: document.querySelector(".teamTwo .trainerName"),
            gameDateAndTime: document.querySelector(".kickoffDate"),
            controlIcons: document.querySelector(".controlsContainer")
        })).init();
        tickerView.setOnControlIconsClickedListener(onControlIconsClickedListener);
    }

    function initPlayerLists() {
        teamOneList = (new App.PlayerList({
            template: document.querySelector("#player-entry").innerHTML,
            startList: document.querySelector(".teamOne .startTeam"),
            substList: document.querySelector(".teamOne .substitutes")
        })).init();

        teamTwoList = (new App.PlayerList({
            template: document.querySelector("#player-entry").innerHTML,
            startList: document.querySelector(".teamTwo .startTeam"),
            substList: document.querySelector(".teamTwo .substitutes")
        })).init();
        teamOneList.setOnStartingPlayerClickedListener(onStartingPlayerClicked);
        teamTwoList.setOnStartingPlayerClickedListener(onStartingPlayerClicked);
    }

    function initTimeCounter() {
        firebaseController.getGameStatus(gameID);
    }

    function initScoreCount() {
        score = (new App.Score()).init();
        scoreView = (new App.ScoreView({
            DOMScoreOne: document.getElementById("scoreTeamOne"),
            DOMScoreTwo: document.getElementById("scoreTeamTwo")
        })).init();
    }

    function onScheduleListGameSelected(game) {
        var teams = {};
        gameID = tickerView.computeGameID(game);
        firebaseController.addEventListener("feedNumberCounted", onFeedNumberCounted);
        firebaseController.addEventListener("gameStatusRetrieved", onGameStatusRetrieved);
        mainView.switchBetweenViews(scheduleScreen, tickerScreen);
        teams.teamOne = firebaseController.getTeamPlayersById(game.teamHomeId, rawTeamsData);
        teams.teamTwo = firebaseController.getTeamPlayersById(game.teamGuestId, rawTeamsData);
        insertTickerData(game, teams);
        initTimeCounter();
        firebaseController.checkForTeamsInDatabase(gameID, game, rawTeamsData);
        firebaseController.getSavedFeedEntries(gameID);
        firebaseController.getFeedNumberOfGame(gameID);
        firebaseController.setChangeListenerOnGame(gameID);
        firebaseController.addEventListener("tickerFeedRetrieved", onTickerFeedRetrieved);
        firebaseController.checkForTeamChanges(gameID);
    }

    function insertTickerData(game, teamsData) {
        eventList = (new App.EventList({
            root: gameID,
            eventList: document.querySelector(".feed")
        })).init();

        eventList.addEventListener("eventPublished", onEventPublished);
        eventList.addEventListener("eventCreationCanceled", onEventCreationCanceled);

        tickerView.insertGameInfo(game, teamsData);

        teamOneList.insertPlayers(teamsData.teamOne);
        teamTwoList.insertPlayers(teamsData.teamTwo);
    }

    function addHighlightFromLists() {
        highlightPanel.classList.remove("hidden");
        teamOneList.toggleHighlight();
        teamTwoList.toggleHighlight();
    }

    function removeHighlightFromLists() {
        highlightPanel.classList.add("hidden");
        teamOneList.toggleHighlight();
        teamTwoList.toggleHighlight();
    }

    function onControlIconsClickedListener(iconType) {
        var gameStatus = timeCounter.getCurrentGameStatus();
        playerOne = null;
        playerTwo = null;
        if (iconType === "whistle" || iconType === "text") {
            if((iconType === "whistle" && !checker.gameHasEnded(gameStatus)) || iconType === "text") {
                constructEventAndShow(iconType, playerOne, playerTwo, gameRunning);
            }
            else {
                tickerView.removeAllIconSelectors();
                triggerAlert();
            }
        } else {
            if (checker.gameRunning(gameStatus)) {
                clickedIconType = iconType;
                addHighlightFromLists();
                makeStartingPlayersClickable();
            } else {
                tickerView.removeAllIconSelectors();
                triggerAlert();
            }
        }
    }

    function makeStartingPlayersClickable() {
        teamOneList.makeStartingPlayersClickable();
        teamTwoList.makeStartingPlayersClickable();
    }

    function makeStartingPlayersUnclickable() {
        teamOneList.makeStartingPlayersUnclickable();
        teamTwoList.makeStartingPlayersUnclickable();
    }

    function makeSubPlayersClickable() {
        teamOneList.makeSubPlayersClickable();
        teamTwoList.makeSubPlayersClickable();
    }

    function makeSubPlayersUnclickable() {
        teamOneList.makeSubPlayersUnclickable();
        teamTwoList.makeSubPlayersUnclickable();
    }

    function onStartingPlayerClicked(playerName) {
        if (clickedIconType === "goal" || clickedIconType === "yellow" || clickedIconType === "red" || clickedIconType === "chance") {
            if (clickedIconType === "goal") {
                //hochzählen
                if (firebaseController.containsPlayer(tickerView.getTeamByID(0), playerName, rawTeamsData)) {
                    score.teamScores(0);
                } else if (firebaseController.containsPlayer(tickerView.getTeamByID(1), playerName, rawTeamsData)) {
                    score.teamScores(1);
                }
            }
            playerOne = playerName;
            removeHighlightFromLists();
            constructEventAndShow(clickedIconType, playerOne, playerTwo, gameRunning);
            makeStartingPlayersUnclickable();
        }
        if (clickedIconType === "foul") {
            if (playerOne === null) {
                playerOne = playerName;
                return;
            } else {
                playerTwo = playerName;

                if (!playersOfSameTeam(playerOne, playerTwo)) {
                    removeHighlightFromLists();
                    constructEventAndShow(clickedIconType, playerOne, playerTwo, gameRunning);
                } else {
                    triggerAlert();
                    playerTwo = null;
                    removeHighlightFromLists();
                    tickerView.removeAllIconSelectors();
                }

            }
        }
        if (clickedIconType === "sub") {
            if (playerOne === null) {
                playerOne = playerName;
                makeStartingPlayersUnclickable();
                makeSubPlayersClickable();
                return;
            } else {
                playerTwo = playerName;

                if (playersOfSameTeam(playerOne, playerTwo)) {
                    removeHighlightFromLists();
                    constructEventAndShow(clickedIconType, playerOne, playerTwo, gameRunning);
                } else {
                    triggerAlert();
                    removeHighlightFromLists();
                    tickerView.removeAllIconSelectors();
                }

            }
        }
        makeStartingPlayersUnclickable();
        makeSubPlayersUnclickable();
        playerOne = null;
    }

    function playersOfSameTeam(playerOne, playerTwo) {
        var teamOne = tickerView.getTeamByID(0),
            teamTwo = tickerView.getTeamByID(1);

        if (firebaseController.containsPlayer(teamOne, playerOne, rawTeamsData) && firebaseController.containsPlayer(teamOne, playerTwo, rawTeamsData)) {
            return true;
        } else if (firebaseController.containsPlayer(teamTwo, playerOne, rawTeamsData) && firebaseController.containsPlayer(teamTwo, playerTwo, rawTeamsData)) {
            return true;
        } else {
            return false;
        }
    }

    function triggerAlert() {
        $(".alert").removeClass("alertHidden");
        setTimeout(function () {
            $(".alert").addClass("alertHidden");
        }, 3000);
    }

    function constructEventAndShow(eventType, playerOne, playerTwo, gameRunning) {
        var event, time = timeCounter.getTime();
        event = (new App.Event({
            time: time,
            type: eventType,
            playerOne: playerOne,
            playerTwo: playerTwo,
            gameRunning: gameRunning,
            scoreTeamOne: score.getScoreByTeamID(0),
            scoreTeamTwo: score.getScoreByTeamID(1)
        })).init();
        eventList.addEventListEntryByTemplate(event.getEvent(), document.getElementById("feed-entry-edit").innerHTML);
    }

    function initListeners() {
        firebaseController.addEventListener("loginStatusChanged", onLoginStatusChanged);
        firebaseController.addEventListener("loginError", onLoginError);
        firebaseController.addEventListener("dataRetrieved", onDataRetrieved);
        firebaseController.addEventListener("savedDataRetrieved", onSavedDataRetrieved);
        firebaseController.addEventListener("teamsStatusRetrieved", onTeamsStatusRetrieved);
        firebaseController.addEventListener("teamPlayersRetrieved", onTeamPlayersRetrieved);
        firebaseController.addEventListener("startingPlayerHighlighting", onStartingPlayerHighlighting);

        loginView.addEventListener("onLoginButtonClicked", tryLogin);

        logoutButtons[0].addEventListener("click", onLogoutButtonClick);
        logoutButtons[1].addEventListener("click", onLogoutButtonClick);

        backButton.addEventListener("click", onBackButtonClick);
    }

    function onSavedDataRetrieved(event) {
        var savedFeeds = event.data,
            i;
        eventList.clearList();
        if (savedFeeds != null) {
            for (i = 0; i < savedFeeds.length; i++) {
                scoreView.updateScore(savedFeeds[i].scoreTeamOne, savedFeeds[i].scoreTeamTwo);
                eventList.addEventListEntryByTemplate(savedFeeds[i], document.getElementById("feed-entry-finished").innerHTML);
            }
        }

    }

    function onDataRetrieved(event) {
        if (event.data.root === "Games") {
            rawGamesData = event.data;
        } else if (event.data.root === "Teams") {
            rawTeamsData = event.data;
        }

        if (rawGamesData !== undefined && rawTeamsData !== undefined) {
            if (rawDataRetrieved === false) {
                computedGamesData = firebaseController.getComputedGamesData(rawGamesData, rawTeamsData);
                $(".playingSchedule .loadingGif").addClass("hidden");
                scheduleList.insertGameData(computedGamesData);
                rawDataRetrieved = true;
            }
        }
    }

    function onTickerFeedRetrieved(event) {
        var newFeedEntry = event.data;
        scoreView.updateScore(newFeedEntry.scoreTeamOne, newFeedEntry.scoreTeamTwo);
        eventList.addEventListEntryByTemplate(newFeedEntry, document.getElementById("feed-entry-finished").innerHTML);
    }

    function onFeedNumberCounted(event) {
        eventList.setEventStart(event.data);
    }

    function onLoginStatusChanged(event) {
        var loginSuccessful = event.data;
        if (loginSuccessful === true) {
            loginView.clearInputs();
            loginView.hideLoginErrorMessage();
            loginView.showLoginInputs();
            loginView.removeListeners();
            mainView.switchBetweenViews(loginScreen, scheduleScreen);

            if ($(".scheduleList").length <= 1) {
                $(".playingSchedule .loadingGif").removeClass("hidden");
            }
            firebaseController.getDataFromRoot("Games");
            firebaseController.getDataFromRoot("Teams");
        } else {
            loginView.hideLoginErrorMessage();
            loginView.showLoginInputs();
            loginView.initListeners();
            mainView.switchToView(loginScreen);
        }
    }

    function onLoginError() {
        loginView.showLoginErrorMessage();
        loginView.showLoginInputs();
    }

    function onEventPublished(event) {
        var data = event.data;
        firebaseController.writeEventIntoTicker(data);
        if (data.type === "whistle") {
            timeCounter.count(false);
            if (gameRunning === true) {
                gameRunning = false;
            } else {
                gameRunning = true;
            }
            timeCounter.setTime();
            firebaseController.writeGameStatusIntoDatabase(timeCounter.getCurrentGameStatus());
        } else if (data.type === "goal" || data.type === "yellow" || data.type === "red" || data.type === "sub") {
            firebaseController.startPlayerHighlighting(gameID, data);
        }
        tickerView.removeAllIconSelectors();
    }

    function onEventCreationCanceled() {
        tickerView.removeAllIconSelectors();
    }

    function initFirebaseConnection() {
        firebaseController = new App.FirebaseController();
        firebaseController.init();
        rawDataRetrieved = false;
    }

    function initDOMElements() {
        loginScreen = document.querySelector(".loginScreen");
        scheduleScreen = document.querySelector(".playingSchedule");
        tickerScreen = document.querySelector(".ticker");
        logoutButtons = document.getElementsByClassName("logoutButton");
        highlightPanel = document.querySelector(".playerListHighlightPanel");
        backButton = document.querySelector(".backButton");
    }

    function tryLogin(event) {
        loginView.showLoadingScreen();
        firebaseController.signIn(event.data.username, event.data.password);
    }

    function onLogoutButtonClick() {
        firebaseController.signOut();
    }

    function onBackButtonClick() {
        mainView.switchToView(scheduleScreen);
        eventList.clearList();
        scoreView.clearScore();
        score.resetScore();

        eventList.removeEventListener("eventPublished", onEventPublished);
        eventList.removeEventListener("eventCreationCanceled", onEventCreationCanceled);
        firebaseController.removeEventListener("tickerFeedRetrieved", onTickerFeedRetrieved);

        firebaseController.removeEventListener("feedNumberCounted", onFeedNumberCounted);
        firebaseController.removeEventListener("gameStatusRetrieved", onGameStatusRetrieved);

        timeCounter.resetCounter();
    }

    function handleImpressModal() {
        var impressModal = new App.ModalController(
            document.getElementsByClassName("impressButton"),
            document.querySelector(".impressModal"));
        impressModal.enable();
    }

    function onGameStatusRetrieved(gameStatus) {
        if (gameStatus.data !== null) {
            timeCounter = (new App.TimeCounter({
                time: document.getElementById("gameTime"),
                root: gameID,
                gameTime: gameStatus.data.time,
                halftime: gameStatus.data.halftime,
                gameStarted: gameStatus.data.gameStarted
            })).init();
            timeCounter.count(true);
            gameRunning = gameStatus.data.gameStarted;
        } else {
            timeCounter = (new App.TimeCounter({
                time: document.getElementById("gameTime"),
                root: gameID,
                gameTime: 1,
                halftime: null,
                gameStarted: null
            })).init();
            //timeCounter.count(true);
        }
    }

    function onTeamsStatusRetrieved(result) {
        var teamsStatus = result.data;
        if (teamsStatus.data === null) {
            firebaseController.writeTeamsIntoDatabase(teamsStatus.root, teamsStatus.game, teamsStatus.rawTeamsData);
        }
    }

    function onTeamPlayersRetrieved(result) {
        var team1Array, team2Array, teamPlayers = result.data.data;
        if (teamPlayers !== null) {

            //http://stackoverflow.com/questions/6857468/converting-a-js-object-to-an-array
            team1Array = $.map(teamPlayers.team1, function (value) {
                return [value];
            });
            team2Array = $.map(teamPlayers.team2, function (value) {
                return [value];
            });
            team1Array.pop();
            team2Array.pop();

            if (teamPlayers !== null) {
                teamOneList.insertPlayers(team1Array);
                teamTwoList.insertPlayers(team2Array);
            }
        }
    }

    function onStartingPlayerHighlighting(result) {
        var index, team1Array, team2Array, eventType, playerToSub,
            playerIndexToHighlight, teamIn, playerIndexToSub,
            eventData = result.data,
            restData = eventData.data,
            team1 = eventData.team1,
            team2 = eventData.team2,
            playerToHighlight = restData.playerOne;

        team1Array = $.map(team1, function (value) {
            return [value];
        });
        team2Array = $.map(team2, function (value) {
            return [value];
        });

        for (index = 0; index < team1Array.length; index++) {
            if (team1Array[index].name === playerToHighlight) {
                playerIndexToHighlight = index;
                teamIn = "team1";
            }
        }
        if (playerIndexToHighlight === undefined) {
            for (index = 0; index < team2Array.length; index++) {
                if (team2Array[index].name === playerToHighlight) {
                    playerIndexToHighlight = index;
                    teamIn = "team2";
                }
            }
        }
        eventType = restData.type;
        playerToSub = restData.playerTwo;
        if (eventType === "sub") {
            if (teamIn === "team1") {
                for (index = 0; index < team1Array.length; index++) {
                    if (team1Array[index].name === playerToSub) {
                        playerIndexToSub = index;
                    }
                }
            } else if (teamIn === "team2") {
                for (index = 0; index < team2Array.length; index++) {
                    if (team2Array[index].name === playerToSub) {
                        playerIndexToSub = index;
                    }
                }
            }
        }
        firebaseController.highlightPlayer(gameID, eventType, teamIn, playerIndexToHighlight, playerIndexToSub);
    }

    that.init = init;
    return that;
}());
