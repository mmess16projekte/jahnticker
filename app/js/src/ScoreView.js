var App = App || {};
App.ScoreView = (function(options) {
    "use strict";

    var that = {},
        scoreDOMTeamOne,
        scoreDOMTeamTwo;

    function init() {
        scoreDOMTeamOne = options.DOMScoreOne;
        scoreDOMTeamTwo = options.DOMScoreTwo;
        return that;
    }

    function updateScore(scoreOne, scoreTwo) {
        scoreDOMTeamOne.innerHTML = scoreOne;
        scoreDOMTeamTwo.innerHTML = scoreTwo;
    }

    function clearScore() {
        scoreDOMTeamOne.innerHTML = "0";
        scoreDOMTeamTwo.innerHTML = "0";
    }

    that.init = init;
    that.updateScore = updateScore;
    that.clearScore = clearScore;
    return that;
});
