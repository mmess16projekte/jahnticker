var App = App || {};
App.Sounds = (function(options) {
    "use strict";
    var that = {},
        soundPath,
        sound;

    function init() {
        soundPath = options.soundPath;
        sound = new Audio(soundPath);
        return that;
    }

    function play() {
        sound.play();
    }

    that.init = init;
    that.play = play;
    return that;
});
