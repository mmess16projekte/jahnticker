var App = App || {};
App.ModalController = (function(buttons, modal){
    "use strict";
    /*global $*/
    var that = {},
        ESC_KEY = 27;

    function enable() {
        var i;
        for(i = 0; i < buttons.length; i++) {
            buttons[i].addEventListener("click", openModal);
        }

        $(modal).find(".closeModal").click(function(){
            hideModal();
        });

        window.onclick = function(e) {
            if(e.target === modal) {
                hideModal();
            }
        };

        document.onkeydown = function(e) {
            if(e.keyCode === ESC_KEY) {
                hideModal();
            }
        };
    }

    function openModal() {
        modal.style.display = "block";
    }

    function hideModal() {
        modal.style.display = "none";
    }

    that.enable = enable;
    return that;
});
