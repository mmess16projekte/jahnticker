var App = App || {};
App.FirebaseController = (function () {
    "use strict";
    /*global firebase, EventPublisher*/
    var that = new EventPublisher(),
        gamesWithChangeListenerSet = [];

    function init() {
        var config = {
            apiKey: "AIzaSyA5VnboWGIXX7aIP6b3Sgr1tI1ys96UObw",
            authDomain: "jahnticker.firebaseapp.com",
            databaseURL: "https://jahnticker.firebaseio.com",
            storageBucket: "jahnticker.appspot.com"
        };
        firebase.initializeApp(config);
        initListeners();
    }

    function initListeners() {
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                that.notifyAll("loginStatusChanged", true);
            } else {
                that.notifyAll("loginStatusChanged", false);
            }
        });
    }

    function signIn(userName, password) {
        firebase.auth().signInWithEmailAndPassword(userName, password)
            .catch(function () {
                that.notifyAll("loginError");
            });
    }

    function signOut() {
        firebase.auth().signOut();
    }

    function getDataFromRoot(root) {
        var result = {};

        firebase.database().ref(root + "/").once("value").then(function (snapshot) {
            result = snapshot.val();
            result.root = root;
            that.notifyAll("dataRetrieved", result);
        });
    }

    function getGameStatus(root) {
        var result = {};
        firebase.database().ref(root + "/GameStatus/").once("value").then(function (snapshot) {
            result = snapshot.val();
            that.notifyAll("gameStatusRetrieved", result);
        });
    }

    function getSavedFeedEntries(root) {
        firebase.database().ref(root + "/Events/").once("value").then(function (snapshot) {
            that.notifyAll("savedDataRetrieved", snapshot.val());
        });
    }

    function getComputedGamesData(rawData, teamsData) {
        var i, tmpTime, tmpDate;

        for (i = 0; i < rawData.length; i++) {
            tmpTime = rawData[i].time;
            tmpDate = rawData[i].date;

            tmpTime = tmpTime.slice(0, 2) + ":" + tmpTime.slice(2, 4);
            tmpDate = tmpDate.slice(0, 2) + "." + tmpDate.slice(2, 4) + "." + tmpDate.slice(4, 8);
            rawData[i].teamOne = getTeamNameById(rawData[i].teamHomeId, teamsData);
            rawData[i].teamTwo = getTeamNameById(rawData[i].teamGuestId, teamsData);
            rawData[i].imagePathLogoTeamOne = getImgPathById(rawData[i].teamHomeId, teamsData);
            rawData[i].imagePathLogoTeamTwo = getImgPathById(rawData[i].teamGuestId, teamsData);
            rawData[i].time = tmpTime;
            rawData[i].date = tmpDate;
            rawData[i].id = i;
        }

        return rawData;
    }

    function getTeamNameById(id, teamsData) {
        var i, teamName;
        for (i = 0; i < teamsData.length; i++) {
            if (id === teamsData[i].id) {
                teamName = teamsData[i].name;
                break;
            }
        }
        return teamName;
    }

    function getImgPathById(id, teamsData) {
        var i, imgPath;

        for (i = 0; i < teamsData.length; i++) {
            if (id === teamsData[i].id) {
                imgPath = teamsData[i].logoPath;
                break;
            }
        }
        return imgPath;
    }

    function getTeamPlayersById(id, teamData) {
        var i, team;
        for (i = 0; i < teamData.length; i++) {
            if (id === teamData[i].id) {
                team = teamData[i].players;
                team.coach = teamData[i].coach;
                break;
            }
        }
        return team;
    }

    function writeEventIntoTicker(tickerData) {
        firebase.database().ref(tickerData.root + "/Events/" + tickerData.id).set({
            time: tickerData.time,
            title: tickerData.title,
            text: tickerData.text,
            type: tickerData.type,
            scoreTeamOne: tickerData.scoreTeamOne,
            scoreTeamTwo: tickerData.scoreTeamTwo,
            playerOne: tickerData.playerOne,
            playerTwo: tickerData.playerTwo
        });
    }

    function writeGameStatusIntoDatabase(tickerData) {
        firebase.database().ref(tickerData.root + "/GameStatus/").set({
            time: tickerData.time,
            gameStarted: tickerData.gameStarted,
            halftime: tickerData.halftime
        });
    }

    function checkForTeamsInDatabase(root, game, rawTeamsData) {
        var result = {};
        result.root = root;
        result.game = game;
        result.rawTeamsData = rawTeamsData;
        firebase.database().ref(root + "/Teams/").once("value").then(function (snapshot) {
            result.data = snapshot.val();
            that.notifyAll("teamsStatusRetrieved", result);
        });
    }

    function writeTeamsIntoDatabse(root, game, rawTeamsData) {
        var index,
            team1 = getTeamPlayersById(game.teamHomeId, rawTeamsData),
            team2 = getTeamPlayersById(game.teamGuestId, rawTeamsData);

        for (index = 0; index < team1.length; index++) {
            team1[index].cardStatus = 0;
            team1[index].goalStatus = 0;
        }
        for (index = 0; index < team2.length; index++) {
            team2[index].cardStatus = 0;
            team2[index].goalStatus = 0;
        }

        firebase.database().ref(root + "/Teams/").set({
            team1: team1,
            team2: team2
        });
    }

    function setChangeListenerOnGame(root) {
        var index, eventData, obj = {};
        for (index = 0; index < gamesWithChangeListenerSet.length; index++) {
            if (gamesWithChangeListenerSet[index] === root) {
                return;
            }
        }

        firebase.database().ref(root + "/Events/").on("child_added", function (snapshot) {
            eventData = snapshot.val();
            that.notifyAll("tickerFeedRetrieved", eventData);
        });

        firebase.database().ref(root + "/Teams/").on("value", function (snapshot) {
            obj.eventData = eventData;
            obj.data = snapshot.val();
            that.notifyAll("teamPlayersRetrieved", obj);
        });

        gamesWithChangeListenerSet.push(root);
    }

    function checkForTeamChanges(root) {
        var obj = {};
        firebase.database().ref(root + "/Teams/").once("value").then(function (snapshot) {
            obj.data = snapshot.val();
            that.notifyAll("teamPlayersRetrieved", obj);
        });
    }

    function getFeedNumberOfGame(root) {
        firebase.database().ref(root + "/Events/").once("value").then(function (snapshot) {
            if (snapshot.val() !== null) {
                that.notifyAll("feedNumberCounted", snapshot.val().length);
            } else {
                that.notifyAll("feedNumberCounted", 0);
            }
        });
    }

    function containsPlayer(teamName, player, rawTeamsData) {
        var i, players;

        for (i = 0; i < rawTeamsData.length; i++) {
            if (rawTeamsData[i].name === teamName) {
                players = rawTeamsData[i].players;
            }
        }

        for (i = 0; i < players.length; i++) {
            if (player.includes(players[i].nname)) {
                return true;
            }
        }
        return false;
    }

    function startPlayerHighlighting(root, data) {
        var result = {};
        firebase.database().ref(root + "/Teams/").once("value").then(function (snapshot) {
            result = snapshot.val();
            result.data = data;
            that.notifyAll("startingPlayerHighlighting", result);
        });
    }

    function highlightPlayer(root, eventType, teamIn, playerIndexToHighlight, playerIndexToSub) {
        if (eventType === "red") {
            firebase.database().ref(root + "/Teams/" + teamIn + "/" + playerIndexToHighlight + "/cardStatus/").set(
                2
            );
        } else if (eventType === "yellow") {
            firebase.database().ref(root + "/Teams/" + teamIn + "/" + playerIndexToHighlight + "/cardStatus/").set(
                1
            );
        } else if (eventType === "goal") {
            firebase.database().ref(root + "/Teams/" + teamIn + "/" + playerIndexToHighlight + "/goalStatus/").set(
                1
            );
        } else if (eventType === "sub") {
            firebase.database().ref(root + "/Teams/" + teamIn + "/" + playerIndexToHighlight + "/stsp/").set(
                false
            );
            firebase.database().ref(root + "/Teams/" + teamIn + "/" + playerIndexToSub + "/stsp/").set(
                true
            );
        }
    }

    that.init = init;
    that.signIn = signIn;
    that.signOut = signOut;
    that.getDataFromRoot = getDataFromRoot;
    that.getComputedGamesData = getComputedGamesData;
    that.getTeamPlayersById = getTeamPlayersById;
    that.writeEventIntoTicker = writeEventIntoTicker;
    that.setChangeListenerOnGame = setChangeListenerOnGame;
    that.getFeedNumberOfGame = getFeedNumberOfGame;
    that.containsPlayer = containsPlayer;
    that.writeGameStatusIntoDatabase = writeGameStatusIntoDatabase;
    that.getGameStatus = getGameStatus;
    that.getSavedFeedEntries = getSavedFeedEntries;
    that.checkForTeamsInDatabase = checkForTeamsInDatabase;
    that.writeTeamsIntoDatabase = writeTeamsIntoDatabse;
    that.startPlayerHighlighting = startPlayerHighlighting;
    that.highlightPlayer = highlightPlayer;
    that.checkForTeamChanges = checkForTeamChanges;
    return that;
});