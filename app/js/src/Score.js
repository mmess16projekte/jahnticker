var App = App || {};
App.Score = (function() {
    "use strict";

    var that = {},
        scoreTeamOne,
        scoreTeamTwo;

    function init() {
        scoreTeamOne = 0;
        scoreTeamTwo = 0;
        return that;
    }

    function teamScores(teamID) {
        if(teamID === 0) {
            scoreTeamOne++;
        } else if(teamID === 1) {
            scoreTeamTwo++;
        }
    }

    function getScoreByTeamID(teamID) {
        if(teamID === 0) {
            return scoreTeamOne;
        } else if(teamID === 1) {
            return scoreTeamTwo;
        }
        return -1;
    }

    function resetScore() {
        scoreTeamOne = 0;
        scoreTeamTwo = 0;
    }

    that.init = init;
    that.teamScores = teamScores;
    that.getScoreByTeamID = getScoreByTeamID;
    that.resetScore = resetScore;
    return that;
});
