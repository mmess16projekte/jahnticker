var App = App || {};
App.TimeCounter = (function (options) {
    "use strict";
    /*global $*/

    var that = {},
        timeCounter,
        timer,
        time,
        gameStarted,
        halftime,
        gameID,
        actualTime;

    function init() {
        timeCounter = options.time;
        gameID = options.root;
        gameStarted = options.gameStarted;
        halftime = options.halftime;
        time = options.gameTime;
        actualTime = 1;
        return that;
    }

    function count(continueCounting) {
        if (gameStarted === null && halftime === null) {
            startCounting(actualTime);
            gameStarted = true;
            halftime = false;
        } else if (gameStarted && !halftime) {
            if (continueCounting) {
                startCounting(actualTime);
            } else {
                stopCounting();
                halftime = true;
                gameStarted = false;
                actualTime = 46;
            }
        } else if (!gameStarted && halftime) {
            if (continueCounting) {
                stopCounting();
                halftime = true;
                gameStarted = false;
                actualTime = 46;
                $(timeCounter).html(actualTime);
            } else {
                startCounting(actualTime);
                gameStarted = true;
            }
        } else if (gameStarted && halftime) {
            if (continueCounting) {
                startCounting(46);
            } else {
                stopCounting();
                gameStarted = false;
                halftime = false;
            }
        } else if (!gameStarted && !halftime) {
            $(timeCounter).html("--");
        }

    }

    function getTime() {
        return actualTime;
    }

    function startCounting(startTime) {
        var timeToDisplay, elapsed;
        timer = setInterval(function () {
            elapsed = Date.now() - time;
            timeToDisplay = new Date(elapsed);
            actualTime = timeToDisplay.getMinutes() + startTime;
            $(timeCounter).html(actualTime);
        }, 100);
    }

    function stopCounting() {
        clearInterval(timer);
        timer = null;
    }

    function setTime() {
        time = Date.now();
    }

    function getCurrentGameStatus() {
        var currentGameStatus = {};
        currentGameStatus.time = time;
        currentGameStatus.root = gameID;
        currentGameStatus.gameStarted = gameStarted;
        currentGameStatus.halftime = halftime;
        return currentGameStatus;
    }

    function resetCounter() {
        stopCounting();
        $(timeCounter).html(0);
    }

    that.init = init;
    that.count = count;
    that.getTime = getTime;
    that.setTime = setTime;
    that.getCurrentGameStatus = getCurrentGameStatus;
    that.resetCounter = resetCounter;
    return that;
});