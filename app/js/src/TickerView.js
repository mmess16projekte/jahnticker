var App = App || {};
App.TickerView = (function (options) {
    "use strict";
    /*global  EventPublisher, _*/

    var that = new EventPublisher(),
        teamOneNameResult,
        teamTwoNameResult,
        teamOneLogo,
        teamTwoLogo,
        teamOneNameLineUp,
        teamOneCoachName,
        teamTwoNameLineUp,
        teamTwoCoachName,
        gameDateAndTime,
        controlIcons,
        controlIconsClickedListenerCallbacks = [];

    function init() {
        initUI();

        return that;
    }

    function initUI() {
        initGameInfo();
        makeUIClickable();
    }

    function initGameInfo() {
        teamOneNameResult = options.teamOneNameResult;
        teamTwoNameResult = options.teamTwoNameResult;
        teamOneLogo = options.teamOneLogo;
        teamTwoLogo = options.teamTwoLogo;
        teamOneNameLineUp = options.teamOneNameLineUp;
        teamOneCoachName = options.teamOneCoachName;
        teamTwoNameLineUp = options.teamTwoNameLineUp;
        teamTwoCoachName = options.teamTwoCoachName;
        gameDateAndTime = options.gameDateAndTime;

        controlIcons = options.controlIcons;
    }

    function makeUIClickable() {
        controlIcons.addEventListener("click", onControlIconsClickedListener);
    }

    function onControlIconsClickedListener(event) {
        var icon;
        icon = event.target;
        if (icon.classList.contains("controlIcon")) {
            icon.classList.add("controlIconSelected");
            _.each(controlIconsClickedListenerCallbacks, function (callback) {
                callback(icon.getAttribute("type"));
            });
        }
    }

    function removeAllIconSelectors() {
        var index, controlIcon, children = controlIcons.children;
        for (index = 0; index < children.length; index++) {
            controlIcon = children[index];
            controlIcon.classList.remove("controlIconSelected");
        }
    }

    function setOnControlIconsClickedListener(callback) {
        controlIconsClickedListenerCallbacks.push(callback);
    }

    function insertGameInfo(game, teamsData) {
        teamOneNameResult.innerHTML = game.teamOne;
        teamTwoNameResult.innerHTML = game.teamTwo;
        teamOneLogo.style.backgroundImage = "url(" + game.imagePathLogoTeamOne + ")";
        teamTwoLogo.style.backgroundImage = "url(" + game.imagePathLogoTeamTwo + ")";
        teamOneNameLineUp.innerHTML = game.teamOne;
        teamOneCoachName.innerHTML = teamsData.teamOne.coach;
        teamTwoNameLineUp.innerHTML = game.teamTwo;
        teamTwoCoachName.innerHTML = teamsData.teamTwo.coach;
        gameDateAndTime.innerHTML = game.date + ", " + game.time;
    }

    function computeGameID(game) {
        var date = game.date,
            time = game.time,
            id;

        date = date.replace(/\./g, "");
        time = time.replace(/\:/g, "");

        id = date + "" + time;

        return id;
    }

    function getTeamByID(teamID) {
        if (teamID === 0) {
            return teamOneNameResult.innerHTML;
        } else if (teamID === 1) {
            return teamTwoNameResult.innerHTML;
        }
        return -1;
    }

    that.init = init;
    that.insertGameInfo = insertGameInfo;
    that.setOnControlIconsClickedListener = setOnControlIconsClickedListener;
    that.computeGameID = computeGameID;
    that.getTeamByID = getTeamByID;
    that.removeAllIconSelectors = removeAllIconSelectors;
    return that;
});