var App = App || {};
App.Notifications = (function() {
    "use strict";

    var that = {};

    function init() {
        document.addEventListener("DOMContentLoaded", function () {
            if (Notification.permission !== "granted") {
                Notification.requestPermission();
            }
        });
        return that;
    }

    function castNotification(data) {
        var notification;

        if (Notification.permission !== "granted") {
            Notification.requestPermission();
        }
        else {
            notification = new Notification("" + data.time + ". min: " + data.title + "", {
                icon: getIcon(data.type),
                body: data.text
            });
        }
    }

    function getIcon(type) {
        var icon;
        switch(type){
        case "whistle":
            icon = "/res/img/icons/notificationIcons/whistle.png";
            break;
        case "text":
            icon = "/res/img/icons/notificationIcons/freeText.png";
            break;
        case "goal":
            icon = "/res/img/icons/notificationIcons/goal.png";
            break;
        case "yellow":
            icon = "/res/img/icons/notificationIcons/yellowCard.png";
            break;
        case "red":
            icon = "/res/img/icons/notificationIcons/redCard.png";
            break;
        case "foul":
            icon = "/res/img/icons/notificationIcons/foul.png";
            break;
        case "chance":
            icon = "/res/img/icons/notificationIcons/chance.png";
            break;
        case "sub":
            icon = "/res/img/icons/notificationIcons/exchange.png";
            break;
        default:
            break;
        }

        return icon;
    }

    that.init = init;
    that.castNotification = castNotification;
    return that;
});
