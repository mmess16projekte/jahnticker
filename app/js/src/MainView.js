var App = App || {};
App.MainView = (function() {
    "use strict";
    /*global $*/

    var that = {},
        SLIDE_TIME = 500;

    function switchBetweenViews(oldView, newView) {
        $(oldView).slideUp(SLIDE_TIME);
        $(newView).slideDown(SLIDE_TIME);
    }

    function switchToView(newView) {
        $(newView).slideDown(SLIDE_TIME);
    }

    that.switchBetweenViews = switchBetweenViews;
    that.switchToView = switchToView;
    return that;
});
