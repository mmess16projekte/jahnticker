var App = App || {};
App.ScheduleList = (function (options) {
    "use strict";
    /*global  EventPublisher, _*/

    var that = new EventPublisher(),
        createGameTemplate,
        scheduleList,
        gameDataArray = [],
        scheduleListGameSelectedCallbacks = [];

    function init() {
        createGameTemplate = _.template(options.entryGameTemplateContent);
        scheduleList = options.scheduleList;
        scheduleList.addEventListener("click", onScheduleListGameSelectedListener);
        return that;
    }

    function addScheduleEntry(game) {
        var entryNode = document.createElement("div");
        entryNode.innerHTML = createGameTemplate(game);
        entryNode.children[0].querySelector(".logoTeamOne").style.backgroundImage = "url(" + game.imagePathLogoTeamOne + ")";
        entryNode.children[0].querySelector(".logoTeamTwo").style.backgroundImage = "url(" + game.imagePathLogoTeamTwo + ")";
        scheduleList.appendChild(entryNode.children[0]);
    }

    function clearList() {
        while (scheduleList.firstChild) {
            scheduleList.removeChild(scheduleList.firstChild);
        }
    }

    function insertGameData(gameData) {
        var index;
        gameDataArray = gameData;
        clearList();
        for (index = 0; index < gameData.length; index++) {
            addScheduleEntry(gameData[index]);
        }
    }

    function onScheduleListGameSelectedListener(event) {
        var gameSelected, game, target = event.target;
        if (target.className !== "scheduleList") {
            if (event.target.className !== "scheduleEntry") {
                gameSelected = findParentWithClass(event.target, "scheduleEntry");
            } else {
                gameSelected = event.target;
            }
            game = gameDataArray[gameSelected.getAttribute("id")];
            _.each(scheduleListGameSelectedCallbacks, function (callback) {
                callback(game);
            });
        }
    }

    function findParentWithClass(child, targetClass) {
        var parent = child.parentElement;
        while (!parent.classList.contains(targetClass)) {
            parent = parent.parentElement;
            if (parent === undefined) {
                break;
            }
        }
        return parent;
    }

    function setOnScheduleListGameSelectedListener(callback) {
        scheduleListGameSelectedCallbacks.push(callback);
    }

    that.init = init;
    that.insertGameData = insertGameData;
    that.setOnScheduleListGameSelectedListener = setOnScheduleListGameSelectedListener;
    return that;
});