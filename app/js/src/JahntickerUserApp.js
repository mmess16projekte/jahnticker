var App = App || {};
App.JahntickerUserApp = (function () {
    "use strict";
    /*global $*/

    var that = {},
        firebaseController,
        notification,
        sound,
        eventList,
        tickerView,
        scoreView,
        teamOneList,
        teamTwoList,
        feedTemplate,
        scheduleList,
        rawGamesData,
        rawTeamsData,
        computedGamesData,
        gameID,
        backButton,
        mainView,
        optionsView,
        scheduleScreen,
        tickerScreen,
        savedDataRetrieved = false;

    function init() {
        showLoadingIcon();
        getDOMElements();
        initViews();
        initFirebaseConnection();
        initListeners();
        handleModals();
    }

    function showLoadingIcon() {
        if ($(".scheduleList").length <= 1) {
            $(".loadingGif").removeClass("hidden");
        }
    }

    function getDOMElements() {
        feedTemplate = document.getElementById("feed-entry-finished").innerHTML;
        backButton = document.querySelector(".backButton");
        scheduleScreen = document.querySelector(".playingSchedule");
        tickerScreen = document.querySelector(".ticker");
    }

    function initViews() {
        mainView = new App.MainView();
        notification = new(App.Notifications()).init();
        sound = new(App.Sounds({
            soundPath: "/res/sounds/cheering.mp3"
        })).init();

        initScheduleList();
        initEventList();
        initTickerView();
        initPlayerLists();
        initScoreView();
        initOptionsView();
    }

    function initScheduleList() {
        scheduleList = (new App.ScheduleList({
            entryGameTemplateContent: document.querySelector("#schedule-entry").innerHTML,
            scheduleList: document.querySelector(".scheduleList")
        })).init();
        scheduleList.setOnScheduleListGameSelectedListener(onScheduleListGameSelected);
    }

    function initTickerView() {
        tickerView = (new App.TickerView({
            teamOneNameResult: document.getElementById("teamOneName"),
            teamTwoNameResult: document.getElementById("teamTwoName"),
            teamOneLogo: document.getElementById("logoTeamOne"),
            teamTwoLogo: document.getElementById("logoTeamTwo"),
            teamOneNameLineUp: document.getElementById("teamOneNameLineUp"),
            teamOneCoachName: document.querySelector(".teamOne .trainerName"),
            teamTwoNameLineUp: document.getElementById("teamTwoNameLineUp"),
            teamTwoCoachName: document.querySelector(".teamTwo .trainerName"),
            gameDateAndTime: document.querySelector(".kickoffDate"),
            controlIcons: document.querySelector(".controlsContainer")
        })).init();
    }

    function initPlayerLists() {
        teamOneList = (new App.PlayerList({
            template: document.querySelector("#player-entry").innerHTML,
            startList: document.querySelector(".teamOne .startTeam"),
            substList: document.querySelector(".teamOne .substitutes")
        })).init();

        teamTwoList = (new App.PlayerList({
            template: document.querySelector("#player-entry").innerHTML,
            startList: document.querySelector(".teamTwo .startTeam"),
            substList: document.querySelector(".teamTwo .substitutes")
        })).init();
    }

    function initScoreView() {
        scoreView = (new App.ScoreView({
            DOMScoreOne: document.getElementById("scoreTeamOne"),
            DOMScoreTwo: document.getElementById("scoreTeamTwo")
        })).init();
    }

    function initOptionsView() {
        optionsView = (new App.OptionView({
            modal: document.querySelector(".optionsModal"),
            closeButton: document.querySelector(".closeModal"),
            closeBigButton: document.querySelector(".modalContainer input[type=button]"),
            notificationCheckbox: document.querySelector(".modalContainer input[name=notificationToggle]"),
            soundCheckbox: document.querySelector(".modalContainer input[name=soundToggle]")
        })).init();
    }

    function onScheduleListGameSelected(game) {
        var teams = {};

        savedDataRetrieved = false;
        mainView.switchBetweenViews(scheduleScreen, tickerScreen);

        teams.teamOne = firebaseController.getTeamPlayersById(game.teamHomeId, rawTeamsData);
        teams.teamTwo = firebaseController.getTeamPlayersById(game.teamGuestId, rawTeamsData);

        insertTickerData(game, teams);
        firebaseController.getSavedFeedEntries(gameID);
        firebaseController.setChangeListenerOnGame(gameID);

        firebaseController.checkForTeamChanges(gameID);
    }

    function insertTickerData(game, teamsData) {
        gameID = tickerView.computeGameID(game);

        tickerView.insertGameInfo(game, teamsData);
        teamOneList.insertPlayers(teamsData.teamOne);
        teamTwoList.insertPlayers(teamsData.teamTwo);
    }

    function initFirebaseConnection() {
        firebaseController = new App.FirebaseController();
        firebaseController.init();
        firebaseController.getDataFromRoot("Games");
        firebaseController.getDataFromRoot("Teams");
    }

    function initEventList() {
        eventList = (new App.EventList({
            root: gameID,
            eventList: document.querySelector(".feed")
        })).init();
    }

    function initListeners() {
        firebaseController.addEventListener("savedDataRetrieved", onSavedDataRetrieved);
        firebaseController.addEventListener("tickerFeedRetrieved", onTickerFeedRetrieved);
        firebaseController.addEventListener("teamPlayersRetrieved", onTeamPlayersRetrieved);
        firebaseController.addEventListener("dataRetrieved", onDataRetrieved);
        backButton.addEventListener("click", onBackButtonClick);
    }

    function onBackButtonClick() {
        savedDataRetrieved = false;
        mainView.switchToView(scheduleScreen);
        eventList.clearList();
        scoreView.clearScore();
    }

    function onSavedDataRetrieved(event) {
        var savedFeeds = event.data,
            i;

        eventList.clearList();

        if (savedFeeds != null) {
            for (i = 0; i < savedFeeds.length; i++) {
                scoreView.updateScore(savedFeeds[i].scoreTeamOne, savedFeeds[i].scoreTeamTwo);
                eventList.addEventListEntryByTemplate(savedFeeds[i], feedTemplate);
                if (i === (savedFeeds.length - 1)) {
                    savedDataRetrieved = true;
                }
            }
        }

    }

    function onTickerFeedRetrieved(event) {
        var newFeedEntry = event.data,
            options = optionsView.getOptions();

        scoreView.updateScore(newFeedEntry.scoreTeamOne, newFeedEntry.scoreTeamTwo);
        eventList.addEventListEntryByTemplate(newFeedEntry, feedTemplate);
        if (options.notifications && savedDataRetrieved) {
            notification.castNotification(newFeedEntry);
        }
        if (options.sounds && savedDataRetrieved) {
            sound.play();
        }
    }

    function onTeamPlayersRetrieved(event) {
        var team1Array, team2Array, teamPlayers = event.data.data;
        if (teamPlayers !== null) {

            //http://stackoverflow.com/questions/6857468/converting-a-js-object-to-an-array
            team1Array = $.map(teamPlayers.team1, function (value) {
                return [value];
            });
            team2Array = $.map(teamPlayers.team2, function (value) {
                return [value];
            });
            team1Array.pop();
            team2Array.pop();

            if (teamPlayers !== null) {
                teamOneList.insertPlayers(team1Array);
                teamTwoList.insertPlayers(team2Array);
            }
        }
    }

    function onDataRetrieved(event) {
        if (event.data.root === "Games") {
            rawGamesData = event.data;
        } else if (event.data.root === "Teams") {
            rawTeamsData = event.data;
        }

        if (rawGamesData !== undefined && rawTeamsData !== undefined) {
            computedGamesData = firebaseController.getComputedGamesData(rawGamesData, rawTeamsData);
            $(".loadingGif").addClass("hidden");
            scheduleList.insertGameData(computedGamesData);
        }
    }

    function handleModals() {
        var impressModal = new App.ModalController(
                document.getElementsByClassName("impressButton"),
                document.querySelector(".impressModal")),
            optionsModal = new App.ModalController(
                document.getElementsByClassName("optionsButton"),
                document.querySelector(".optionsModal")
            );
        impressModal.enable();
        optionsModal.enable();
    }

    that.init = init;
    return that;
}());
