var App = App || {};
App.LoginView = (function(options) {
    "use strict";
    /*global EventPublisher*/

    var that = new EventPublisher(),
        usernameInput,
        passwordInput,
        loginButton,
        loadingView,
        inputsView,
        loginErrorText;

    function init() {
        getElements();
        usernameInput.focus();
        return that;
    }

    function clearInputs() {
        usernameInput.value = "";
        passwordInput.value = "";
    }

    function showLoadingScreen() {
        inputsView.classList.add("hidden");
        loadingView.classList.remove("hidden");
    }

    function showLoginInputs() {
        inputsView.classList.remove("hidden");
        loadingView.classList.add("hidden");
    }

    function showLoginErrorMessage() {
        loginErrorText.classList.remove("hidden");
    }

    function hideLoginErrorMessage() {
        loginErrorText.classList.add("hidden");
    }

    function getElements() {
        usernameInput = options.usernameInput;
        passwordInput = options.passwordInput;
        loginButton = options.loginButton;
        loadingView = options.loadingView;
        inputsView = options.inputsView;
        loginErrorText = options.loginErrorText;
    }

    function initListeners() {
        var KEY_ENTER = 13;
        loginButton.addEventListener("click", onLoginClicked);
        document.onkeydown = function (event) {
            if (event.keyCode === KEY_ENTER) {
                onLoginClicked();
            }
        };
    }

    function removeListeners() {
        loginButton.removeEventListener("click", onLoginClicked);
        document.onkeydown = null;
    }

    function onLoginClicked() {
        var loginInfos = {
            username: usernameInput.value,
            password: passwordInput.value
        };
        that.notifyAll("onLoginButtonClicked", loginInfos);
    }

    that.init = init;
    that.initListeners = initListeners;
    that.removeListeners = removeListeners;
    that.clearInputs = clearInputs;
    that.showLoadingScreen = showLoadingScreen;
    that.showLoginInputs = showLoginInputs;
    that.showLoginErrorMessage = showLoginErrorMessage;
    that.hideLoginErrorMessage = hideLoginErrorMessage;
    return that;
});
