var App = App || {};
App.PlayerList = (function (options) {
    "use strict";
    /*global  EventPublisher, _, $*/

    var that = new EventPublisher(),
        bindingFunction,
        startTeamList,
        substitutesList,
        playersInList,
        startingPlayerClickedCallbacks = [];

    function init() {
        bindingFunction = _.template(options.template);
        startTeamList = options.startList;
        substitutesList = options.substList;
        return that;
    }

    function swapPlayers(playerOut, playerIn) {
        if (getPlayerIndex(playerIn) !== -1 || getPlayerIndex(playerOut) !== -1) {
            playersInList[getPlayerIndex(playerIn)].stsp = true;
            playersInList[getPlayerIndex(playerOut)].stsp = false;
            clearLists();
            insertPlayers(playersInList);
        }
    }

    function getPlayerIndex(playerName) {
        var index;
        for (index = 0; index < playersInList.length; index++) {
            if (playerName === playersInList[index].name) {
                return index;
            }
        }
        return -1;
    }

    function insertPlayers(teamData) {
        var i;
        playersInList = teamData;
        clearLists();
        for (i = 0; i < playersInList.length; i++) {
            addPlayerListEntry(playersInList[i]);
        }
    }

    function clearLists() {
        while (startTeamList.firstChild) {
            startTeamList.removeChild(startTeamList.firstChild);
        }

        while (substitutesList.firstChild) {
            substitutesList.removeChild(substitutesList.firstChild);
        }
    }

    function addPlayerListEntry(player) {
        var tmpElement = document.createElement("div");
        if (player.name === undefined) {
            player.name = player.nname + " " + player.vname.substring(0, 1) + ".";
        }
        tmpElement.innerHTML = bindingFunction(player);

        if (player.cardStatus === 1) {
            tmpElement.children[0].querySelector("#yellowCard").classList.remove("hidden");
        }
        if (player.cardStatus === 2) {
            tmpElement.children[0].querySelector("#redCard").classList.remove("hidden");
        }

        if (player.goalStatus === 1) {
            tmpElement.children[0].querySelector("#goal").classList.remove("hidden");
        }

        if (player.stsp === true) {
            startTeamList.appendChild(tmpElement.children[0]);
        } else {
            substitutesList.appendChild(tmpElement.children[0]);
        }
    }

    function makeStartingPlayersClickable() {
        startTeamList.addEventListener("click", onStartingPlayerClicked);
    }

    function makeStartingPlayersUnclickable() {
        startTeamList.removeEventListener("click", onStartingPlayerClicked);
    }

    function makeSubPlayersClickable() {
        substitutesList.addEventListener("click", onStartingPlayerClicked);
    }

    function makeSubPlayersUnclickable() {
        substitutesList.removeEventListener("click", onStartingPlayerClicked);
    }

    function setOnStartingPlayerClickedListener(callback) {
        startingPlayerClickedCallbacks.push(callback);
    }

    function onStartingPlayerClicked(event) {
        var listElement, playerName;
        if (event.target.className !== "startTeam playerList") {
            if (event.target.className === "playerEntry") {
                listElement = event.target;
            } else {
                listElement = findParentWithClass(event.target, "playerEntry");
            }
            playerName = listElement.querySelector(".playerName").innerHTML;

            _.each(startingPlayerClickedCallbacks, function (callback) {
                callback(playerName);
            });
        }
    }

    function findParentWithClass(child, targetClass) {
        var parent = child.parentElement;
        while (!parent.classList.contains(targetClass)) {
            parent = parent.parentElement;
            if (parent === undefined) {
                break;
            }
        }
        return parent;
    }

    function toggleHighlight() {
        var teamContainer = $(startTeamList).closest(".teamContainer").get(0);
        teamContainer.classList.toggle("liftUp");
    }

    function highlightPlayer(type, playerName) {
        var index, playerElementToHighlight, startTeamPlayersList = startTeamList.getElementsByClassName("playerName");

        for (index = 0; index < startTeamPlayersList.length; index++) {
            if (startTeamPlayersList[index].innerHTML === playerName) {
                playerElementToHighlight = startTeamPlayersList[index].parentElement;
                break;
            }
        }

        if (playerElementToHighlight !== undefined) {
            if (type === "red") {
                playerElementToHighlight.querySelector("#redCard").classList.remove("hidden");
            } else if (type === "yellow") {
                playerElementToHighlight.querySelector("#yellowCard").classList.remove("hidden");
            } //goal */
        }
    }

    that.init = init;
    that.insertPlayers = insertPlayers;
    that.setOnStartingPlayerClickedListener = setOnStartingPlayerClickedListener;
    that.makeStartingPlayersClickable = makeStartingPlayersClickable;
    that.makeStartingPlayersUnclickable = makeStartingPlayersUnclickable;
    that.makeSubPlayersClickable = makeSubPlayersClickable;
    that.makeSubPlayersUnclickable = makeSubPlayersUnclickable;
    that.swapPlayers = swapPlayers;
    that.toggleHighlight = toggleHighlight;
    that.highlightPlayer = highlightPlayer;
    return that;
});
