var App = App || {};
App.Event = (function (options) {
    "use strict";
    /*global  */

    var that = {},
        time,
        eventType,
        playerOne,
        playerTwo,
        scoreTeamOne,
        scoreTeamTwo,
        title,
        text,
        gameRunning;

    function init() {
        initProperties();
        setTitleAndText();
        return that;
    }

    function initProperties() {
        time = options.time;
        eventType = options.type;
        playerOne = options.playerOne;
        playerTwo = options.playerTwo;
        gameRunning = options.gameRunning;
        scoreTeamOne = options.scoreTeamOne;
        scoreTeamTwo = options.scoreTeamTwo;
    }

    function setTitleAndText() {
        switch (eventType) {
        case "goal":
            title = "Tor";
            text = "Tor! " + playerOne + " schießt den Ball ins Netz!";
            break;
        case "yellow":
            title = "Gelbe Karte";
            text = "Gelbe Karte für " + playerOne + "!";
            break;
        case "red":
            title = "Rote Karte";
            text = "Rot! " + playerOne + " fliegt vom Platz.";
            break;
        case "foul":
            title = "Foul";
            text = playerOne + " foult " + playerTwo + ". Freistoß!";
            break;
        case "chance":
            title = "Chance";
            text = "Chance für " + playerOne + " in der " + time + ". Minute!";
            break;
        case "whistle":
            if (time < 2) {
                title = "Anpfiff";
                text = "Das Spiel beginnt!";
            } else if (time < 60 && gameRunning) {
                title = "Halbzeit";
                text = "Der Schiedsrichter pfeift zur Halbzeit.";
            } else if (time < 60 && !gameRunning) {
                title = "Anpfiff 2. Halbzeit";
                text = "Der Ball rollt wieder!";
            } else if (time > 70) {
                title = "Spielende";
                text = "Das Spiel ist vorbei!";
            }
            break;
        case "text":
            title = "";
            text = "";
            break;
        case "sub":
            title = "Wechsel";
            text = playerTwo + " kommt für " + playerOne + " in die Partie.";
            break;
        default:
            //error
        }
    }

    function getEvent() {
        var result = {};
        result.time = time;
        result.type = eventType;
        result.title = title;
        result.text = text;
        result.scoreTeamOne = scoreTeamOne;
        result.scoreTeamTwo = scoreTeamTwo;
        result.playerOne = playerOne;
        result.playerTwo = playerTwo;
        return result;
    }

    that.init = init;
    that.getEvent = getEvent;
    return that;
});
