Jahnticker - Abschlussprojekt für das Seminar MME
=================================================

Für den Start der Anwendungen sind keine speziellen Vorgehensweisen nötig. Es
ist allerdings zu empfehlen, einen aktuellen Browser zu verwenden. Bei der
Entwicklung wurde Chrome verwendet.  
Anmeldeinformtionen:  
Benutzername: admin@admin.de  
Passwort: admin123  
Der Jahnticker ist unter folgendem Link zu finden:  http://homepages.uni-regensburg.de/~pes02327/jahnticker/