var App = App || {};
App.firebaseController = (function () {
  "use strict";

  var that = {},
    mailInput,
    loginScreen,
    content,
    passwordInput,
    loginButton,
    logoutButton,
    writeButton,
    auth,
    db;

  function init() {
    initFirebase();
    getDOMElements();
    initListeners();
  }

  function initFirebase() {
    var config = {
        apiKey: "AIzaSyAOsm8BmlM0XI76r3gy3m_7c_dc-on7OcE",
        authDomain: "project-7180939328815065502.firebaseapp.com",
        databaseURL: "https://project-7180939328815065502.firebaseio.com",
        storageBucket: "project-7180939328815065502.appspot.com",
    };
    firebase.initializeApp(config);
    auth = firebase.auth();
    db = firebase.database();
    auth.signOut();
  }

  function getDOMElements() {
    loginScreen = document.querySelector(".loginScreen");
    content = document.getElementById("content");
    mailInput = document.getElementById("mailInput");
    passwordInput = document.getElementById("passwordInput");
    document.getElementById("mailInput").focus();

    loginButton = document.getElementById("loginButton");
    logoutButton = document.getElementById("logoutButton");
    writeButton = document.getElementById("writeButton");
  }

  function initListeners() {
    var KEY_ENTER = 13;
    loginButton.addEventListener("click", onLoginButton);
    logoutButton.addEventListener("click", onLogoutButton);
    writeButton.addEventListener("click", onWriteButton);
    loginScreen.addEventListener("webkitTransitionEnd", function() {
      loginScreen.classList.add("hidden");
      content.classList.add("fadeIn");
      }, false);
    document.onkeydown = function (event) {
      if (event.keyCode === KEY_ENTER) {
        onLoginButton();
      }
    };

    auth.onAuthStateChanged(function (user) {
      if (user) {
        console.log(user.email + " is signed in");
        onLogin();
      } else {
        console.log("No user signed in");
      }
    });
  }

  function onLoginButton() {
    var userMail = mailInput.value,
      userPassword = passwordInput.value;

    auth.signInWithEmailAndPassword(userMail, userPassword)
            .catch(function (error) {
              console.log(error.code);
              console.log(error.message);
            });
  }

  function onLogoutButton() {
    $(loginScreen).slideDown(500);
    mailInput.focus();
    auth.signOut();
  }

  function onWriteButton() {
    db.ref("Test/").set({
      bla: "blub",
      blub: "bla",
    });
  }

  function onLogin() {
    var teams = {};
    mailInput.value = "";
    passwordInput.value = "";
    $(loginScreen).slideUp(500);
    $("#content").slideDown(500);
    //loginScreen.classList.add("fadeOut");
    db.ref("Teams/").once("value").then(function (snapshot) {
      teams = snapshot.val();
      showDBContent(teams);
    });
  }

  function showDBContent(teams) {
    var i, j, players, player;

    for (i = 0; i < teams.length; i++) {
      console.log(teams[i].name);
      players = teams[i].players;
      for (j = 0; j < players.length; j++) {
        player = players[j];
        console.log(player.nr, player.vname, player.nname, player.position);
      }
    }
  }

  that.init = init;
  return that;
}());
